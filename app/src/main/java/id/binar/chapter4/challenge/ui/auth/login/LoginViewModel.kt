package id.binar.chapter4.challenge.ui.auth.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.repository.AuthRepository
import id.binar.chapter4.challenge.util.Result
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : ViewModel() {

    private val repository: AuthRepository = AuthRepository(application)

    private val result = MutableLiveData<Result<User>>()

    fun login(email: String, password: String): LiveData<Result<User>> {
        viewModelScope.launch {
            val data = repository.login(email, password)

            if (data == null) {
                result.value = Result.Error("User tidak ditemukan")
            } else {
                result.value = Result.Success(data)
            }
        }

        return result
    }
}