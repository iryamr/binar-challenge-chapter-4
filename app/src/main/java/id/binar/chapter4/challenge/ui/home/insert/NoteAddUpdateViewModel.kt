package id.binar.chapter4.challenge.ui.home.insert

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.repository.NoteRepository
import kotlinx.coroutines.launch

class NoteAddUpdateViewModel(application: Application) : ViewModel() {

    private val repository: NoteRepository = NoteRepository(application)

    private var insert: Long = 0L
    private var update: Int = 0

    fun insertNote(note: Note): Long {
        viewModelScope.launch {
            insert = repository.insertNote(note)
        }
        return insert
    }

    fun updateNote(note: Note): Int {
        viewModelScope.launch {
            update = repository.updateNote(note)
        }
        return update
    }
}