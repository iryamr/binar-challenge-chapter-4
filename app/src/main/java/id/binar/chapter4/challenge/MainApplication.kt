package id.binar.chapter4.challenge

import android.app.Application

class MainApplication : Application() {

    private lateinit var mainAppInstance: MainApplication

    fun getInstance(): MainApplication {
        mainAppInstance = this
        return mainAppInstance
    }
}