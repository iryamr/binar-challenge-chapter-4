package id.binar.chapter4.challenge.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.source.Authenticator
import id.binar.chapter4.challenge.databinding.FragmentLoginBinding
import id.binar.chapter4.challenge.ui.home.HomeActivity
import id.binar.chapter4.challenge.util.Result
import id.binar.chapter4.challenge.util.ViewModelFactory
import id.binar.chapter4.challenge.util.ViewUtils.showToast

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LoginViewModel by viewModels {
        ViewModelFactory.getInstance(
            requireActivity().application
        )
    }

    private val authenticator: Authenticator by lazy { Authenticator(requireContext()) }
    private val observer: Observer<Result<User>> by lazy {
        Observer { result ->
            when (result) {
                is Result.Success -> {
                    authenticator.setLoggedInUser(result.data.username)
                    authenticator.checkAuth()
                    moveToHomeActivity()
                }
                is Result.Error -> {
                    showToast(result.error, requireContext())
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        login()
        register()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun login() {
        binding.btnLogin.setOnClickListener {
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            when {
                email.isEmpty() -> {
                    binding.etlEmail.error = "Email tidak boleh kosong"
                    return@setOnClickListener
                }
                password.isEmpty() -> {
                    binding.etlEmail.error = "Password tidak boleh kosong"
                    return@setOnClickListener
                }
            }

            viewModel.login(email, password).observe(viewLifecycleOwner, observer)
        }
    }

    private fun moveToHomeActivity() {
        Intent(requireContext(), HomeActivity::class.java).also { intent ->
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun register() {
        binding.tvRegister.setOnClickListener {
            it.findNavController()
                .navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
    }
}