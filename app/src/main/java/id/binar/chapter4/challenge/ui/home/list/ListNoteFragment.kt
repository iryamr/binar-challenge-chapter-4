package id.binar.chapter4.challenge.ui.home.list

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import id.binar.chapter4.challenge.R
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.source.Authenticator
import id.binar.chapter4.challenge.databinding.FragmentListNoteBinding
import id.binar.chapter4.challenge.ui.adapter.NoteActionListener
import id.binar.chapter4.challenge.ui.adapter.NoteAdapter
import id.binar.chapter4.challenge.ui.auth.AuthActivity
import id.binar.chapter4.challenge.util.ViewModelFactory
import id.binar.chapter4.challenge.util.ViewUtils.showNoteDialog
import id.binar.chapter4.challenge.util.ViewUtils.showToast

class ListNoteFragment : Fragment() {

    private var _binding: FragmentListNoteBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ListNoteViewModel by viewModels {
        ViewModelFactory.getInstance(
            requireActivity().application
        )
    }

    private val noteAdapter: NoteAdapter by lazy { NoteAdapter(action) }
    private val authenticator: Authenticator by lazy { Authenticator(requireContext()) }
    private val username: String? by lazy { authenticator.getLoggedInUser() }
    private val observer: Observer<List<Note>?> by lazy {
        Observer { result ->
            if (result.isNullOrEmpty()) {
                binding.layoutEmpty.root.isVisible = true
                binding.rvNotes.isVisible = false
                return@Observer
            }
            binding.layoutEmpty.root.isVisible = false
            binding.rvNotes.isVisible = true
            noteAdapter.submitList(result)
        }
    }

    private val action: NoteActionListener = object : NoteActionListener {
        override fun onEdit(note: Note) {
            showNoteDialog(childFragmentManager, note)
        }

        override fun onDelete(note: Note) {
            when (viewModel.deleteNote(note)) {
                0 -> showToast(
                    getString(R.string.text_failed_to_delete_note),
                    requireContext()
                )
                else -> showToast(
                    getString(R.string.text_success_to_delete_note),
                    requireContext()
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListNoteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUsername(username)
        logout()

        createNote()
        showNotes()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUsername(name: String?) {
        binding.tvGreeting.text = getString(R.string.greeting, name)
    }

    private fun showNotes() {
        binding.rvNotes.adapter = noteAdapter
        binding.rvNotes.layoutManager = LinearLayoutManager(requireContext())

        viewModel.getNotes(username).observe(viewLifecycleOwner, observer)
    }

    private fun createNote() {
        binding.btnAdd.setOnClickListener { showNoteDialog(childFragmentManager) }
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            AlertDialog.Builder(requireContext()).apply {
                setTitle(context.getString(R.string.text_alert_logout))
                setMessage(context.getString(R.string.text_question_alert_logout))
                setPositiveButton(context.getString(R.string.text_yes)) { _, _ ->
                    authenticator.clearLoggedInUser()
                    clearActivity()
                }
                setNegativeButton(context.getString(R.string.text_no)) { dialog, _ ->
                    dialog.cancel()
                }
            }.show()
        }
    }

    private fun clearActivity() {
        Intent(requireContext(), AuthActivity::class.java).also {
            startActivity(it)
            requireActivity().finish()
        }
    }
}