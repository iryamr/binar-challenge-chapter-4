package id.binar.chapter4.challenge.data.source

import android.content.Context
import android.content.SharedPreferences

class Authenticator(context: Context) {

    private var prefs: SharedPreferences =
        context.getSharedPreferences("auth_prefs", Context.MODE_PRIVATE)

    fun setLoggedInUser(username: String) {
        val editor = prefs.edit()
        editor.putString("key_logged_in", username)
        editor.apply()
    }

    fun getLoggedInUser(): String? {
        return if (prefs.contains("key_logged_in")) {
            prefs.getString("key_logged_in", null)
        } else {
            clearLoggedInUser()
            null
        }
    }

    fun checkAuth(): String? =
        getLoggedInUser()

    fun clearLoggedInUser() {
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }
}