package id.binar.chapter4.challenge.data.repository

import android.app.Application
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.source.NoteDatabase
import id.binar.chapter4.challenge.data.source.dao.UserDao

class AuthRepository(
    application: Application
) {

    private val userDao: UserDao

    init {
        val db = NoteDatabase.getDatabase(application)
        userDao = db.userDao
    }

    suspend fun login(email: String, password: String) = userDao.login(email, password)

    suspend fun register(user: User?) = userDao.register(user)
}