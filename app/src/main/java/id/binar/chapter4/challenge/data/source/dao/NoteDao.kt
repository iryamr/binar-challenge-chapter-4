package id.binar.chapter4.challenge.data.source.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import id.binar.chapter4.challenge.data.entity.Note

@Dao
interface NoteDao {

    @Query("SELECT * FROM notes WHERE username = :username ORDER BY id DESC")
    fun getNotes(username: String?): LiveData<List<Note>?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(note: Note): Long

    @Update
    suspend fun updateNote(note: Note): Int

    @Delete
    suspend fun deleteNote(note: Note): Int
}