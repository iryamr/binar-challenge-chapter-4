package id.binar.chapter4.challenge.util

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import id.binar.chapter4.challenge.R
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.ui.home.insert.NoteAddUpdateFragment

object ViewUtils {

    fun showNoteDialog(fragmentManager: FragmentManager) {
        val dialog = NoteAddUpdateFragment()
        dialog.show(fragmentManager, NoteAddUpdateFragment::class.java.simpleName)
    }

    fun showNoteDialog(fragmentManager: FragmentManager, note: Note) {
        val dialog = NoteAddUpdateFragment(note)
        dialog.show(fragmentManager, NoteAddUpdateFragment::class.java.simpleName)
    }

    @Suppress("UNUSED_EXPRESSION")
    fun showLogoutDialog(
        context: Context,
        clearLoggedInUser: Unit,
        clearActivity: Unit
    ) {
        AlertDialog.Builder(context).apply {
            setTitle(context.getString(R.string.text_alert_logout))
            setMessage(context.getString(R.string.text_question_alert_logout))
            setPositiveButton(context.getString(R.string.text_yes)) { _, _ ->
                clearLoggedInUser
                clearActivity
            }
            setNegativeButton(context.getString(R.string.text_no)) { dialog, _ ->
                dialog.cancel()
            }
        }.show()
    }

    fun showToast(message: String, context: Context) =
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    fun showLoading(isLoading: Boolean, view: View) {
        view.isVisible = isLoading
    }
}