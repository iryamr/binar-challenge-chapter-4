package id.binar.chapter4.challenge.ui.auth.register

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.source.Authenticator
import id.binar.chapter4.challenge.databinding.FragmentRegisterBinding
import id.binar.chapter4.challenge.ui.home.HomeActivity
import id.binar.chapter4.challenge.util.ViewModelFactory

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: RegisterViewModel by viewModels {
        ViewModelFactory.getInstance(
            requireActivity().application
        )
    }

    private val authenticator: Authenticator by lazy { Authenticator(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        register()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun register() {
        binding.btnRegister.setOnClickListener {
            val username = binding.etName.text.toString()
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()
            val passwordConfirm = binding.etPasswordConfirm.text.toString()

            when {
                username.isEmpty() -> {
                    binding.etlName.error = "Nama tidak boleh kosong"
                    return@setOnClickListener
                }
                email.isEmpty() -> {
                    binding.etlEmail.error = "Username tidak boleh kosong"
                    return@setOnClickListener
                }
                password.isEmpty() -> {
                    binding.etlPassword.error = "Password tidak boleh kosong"
                    return@setOnClickListener
                }
                else -> {
                    if (passwordConfirm != password) {
                        binding.etlPasswordConfirm.error = "Password tidak sesuai"
                        return@setOnClickListener
                    }
                }
            }

            val user = User(
                username = username,
                email = email,
                password = password
            )

            viewModel.register(user)

            authenticator.setLoggedInUser(user.username)
            moveToHomeActivity()
        }
    }

    private fun moveToHomeActivity() {
        Intent(requireContext(), HomeActivity::class.java).also { intent ->
            startActivity(intent)
            requireActivity().finish()
        }
    }
}