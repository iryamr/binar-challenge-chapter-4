package id.binar.chapter4.challenge.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.binar.chapter4.challenge.data.source.Authenticator
import id.binar.chapter4.challenge.databinding.ActivityAuthBinding
import id.binar.chapter4.challenge.ui.home.HomeActivity

class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding

    private val authenticator: Authenticator by lazy { Authenticator(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        if (authenticator.checkAuth() != null) moveToHomeActivity()
    }

    private fun moveToHomeActivity() {
        Intent(this, HomeActivity::class.java).also { intent ->
            startActivity(intent)
            finish()
        }
    }
}