package id.binar.chapter4.challenge.ui.adapter

import id.binar.chapter4.challenge.data.entity.Note

interface NoteActionListener {

    fun onEdit(note: Note)

    fun onDelete(note: Note)
}