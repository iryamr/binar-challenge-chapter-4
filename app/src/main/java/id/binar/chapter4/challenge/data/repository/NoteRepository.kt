package id.binar.chapter4.challenge.data.repository

import android.app.Application
import androidx.lifecycle.LiveData
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.source.NoteDatabase
import id.binar.chapter4.challenge.data.source.dao.NoteDao

class NoteRepository(application: Application) {

    private val noteDao: NoteDao

    init {
        val db = NoteDatabase.getDatabase(application)
        noteDao = db.noteDao
    }

    suspend fun insertNote(note: Note): Long = noteDao.insertNote(note)

    suspend fun updateNote(note: Note): Int = noteDao.updateNote(note)

    fun getNotes(username: String?): LiveData<List<Note>?> = noteDao.getNotes(username)

    suspend fun deleteNote(note: Note): Int = noteDao.deleteNote(note)
}