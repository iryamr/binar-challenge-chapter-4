package id.binar.chapter4.challenge.ui.auth.register

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.repository.AuthRepository
import kotlinx.coroutines.launch

class RegisterViewModel(application: Application) : ViewModel() {

    private val repository: AuthRepository = AuthRepository(application)

    fun register(user: User) {
        viewModelScope.launch {
            repository.register(user)
        }
    }
}