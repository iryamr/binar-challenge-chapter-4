package id.binar.chapter4.challenge.ui.home.list

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.repository.NoteRepository
import kotlinx.coroutines.launch

class ListNoteViewModel(application: Application) : ViewModel() {

    private val repository: NoteRepository = NoteRepository(application)

    private var delete: Int = 0

    fun getNotes(username: String?): LiveData<List<Note>?> = repository.getNotes(username)

    fun deleteNote(note: Note): Int {
        viewModelScope.launch {
            delete = repository.deleteNote(note)
        }
        return delete
    }
}