package id.binar.chapter4.challenge.ui.home.insert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import id.binar.chapter4.challenge.R
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.source.Authenticator
import id.binar.chapter4.challenge.databinding.FragmentNoteAddUpdateBinding
import id.binar.chapter4.challenge.util.ViewModelFactory
import id.binar.chapter4.challenge.util.ViewUtils.showToast

class NoteAddUpdateFragment() : DialogFragment() {

    private var _binding: FragmentNoteAddUpdateBinding? = null
    private val binding get() = _binding!!

    private var isEdit = false
    private var note: Note? = null

    private val viewModel: NoteAddUpdateViewModel by viewModels {
        ViewModelFactory.getInstance(
            requireActivity().application
        )
    }

    private val authenticator: Authenticator by lazy { Authenticator(requireContext()) }

    constructor(note: Note?) : this() {
        this.note = note
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNoteAddUpdateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentNoteAddUpdateBinding.bind(view)

        checkNote()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun getTheme(): Int = R.style.Theme_Dialog

    private fun checkNote() {
        if (note != null) {
            isEdit = true
        } else {
            note = Note()
        }

        if (isEdit) {
            binding.tvDialog.text = getString(R.string.text_edit_data)
            binding.btnSubmit.text = getString(R.string.text_update_data)
            note?.let {
                binding.etTitle.setText(it.title)
                binding.etContent.setText(it.content)
            }
        } else {
            binding.tvDialog.text = getString(R.string.text_input_data)
            binding.btnSubmit.text = getString(R.string.text_save_data)
        }

        saveNote()
    }

    private fun saveNote() {
        binding.btnSubmit.setOnClickListener {
            val title = binding.etTitle.text.toString()
            val content = binding.etContent.text.toString()
            val username = authenticator.getLoggedInUser()

            note?.let { data ->
                data.username = username
                data.title = title
                data.content = content

                if (isEdit) {
                    when (viewModel.updateNote(data)) {
                        0 -> showToast(
                            getString(R.string.text_success_to_update_note),
                            requireContext()
                        )
                        else -> showToast(
                            getString(R.string.text_failed_to_update_note),
                            requireContext()
                        )
                    }
                } else {
                    when (viewModel.insertNote(data)) {
                        0L -> showToast(
                            getString(R.string.text_success_to_create_note),
                            requireContext()
                        )
                        else -> showToast(
                            getString(R.string.text_failed_to_create_note),
                            requireContext()
                        )
                    }
                }
            }

            dialog?.dismiss()
        }
    }
}