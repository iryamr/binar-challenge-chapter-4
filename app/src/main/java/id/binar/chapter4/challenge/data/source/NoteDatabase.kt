package id.binar.chapter4.challenge.data.source

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.binar.chapter4.challenge.data.entity.Note
import id.binar.chapter4.challenge.data.entity.User
import id.binar.chapter4.challenge.data.source.dao.NoteDao
import id.binar.chapter4.challenge.data.source.dao.UserDao

@Database(
    entities = [
        User::class,
        Note::class
    ],
    version = 1,
    exportSchema = false
)
abstract class NoteDatabase : RoomDatabase() {

    abstract val userDao: UserDao

    abstract val noteDao: NoteDao

    companion object {
        @Volatile
        private var INSTANCE: NoteDatabase? = null

        @JvmStatic
        fun getDatabase(context: Context): NoteDatabase {
            if (INSTANCE == null) {
                synchronized(NoteDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        NoteDatabase::class.java,
                        "note_database"
                    ).build()
                }
            }
            return INSTANCE as NoteDatabase
        }
    }

}